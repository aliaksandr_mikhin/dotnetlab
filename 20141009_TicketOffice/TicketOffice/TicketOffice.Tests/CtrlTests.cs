﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using TicketOffice.Domain.Entities;
using TicketOffice.Domain.Enums;
using TicketOffice.Domain.Factories;
using TicketOffice.Domain.Infrastructure;
using TicketOffice.Domain.Providers;
using TicketOffice.Domain.Services;

namespace TicketOffice.Tests
{
	[TestClass]
	public class CtrlTests
	{
		class OnlinePayment : IPaymentProvider
		{
			public PaymentResult Pay(string cardNumber, string order, double sum)
			{
				return new PaymentResult
					{
						Result = PaymentResults.Success,
						Order = order
					};
			}
		}

		RepositoryFactory repositoryFactory;

		[TestInitialize]
		public void InitTests()
		{
			buyersList = new List<Buyer>
				{
					new Buyer { Id = 1, BuyerName = "Alex", CardNumber = "123" },
					new Buyer { Id = 2, BuyerName = "John", CardNumber = "456" },
					new Buyer { Id = 3, BuyerName = "Jane", CardNumber = "789" }
				};

			trainsList = new List<Train>
				{
					new Train { Id = 1, DepartPoint = "Minsk", DestinPoint = "Moscow", TrainNumber = "111" },
					new Train { Id = 2, DepartPoint = "Brest", DestinPoint = "Homel", TrainNumber = "222" },
					new Train { Id = 3, DepartPoint = "Vitebsk", DestinPoint = "Misnk", TrainNumber = "333" },
					new Train { Id = 4, DepartPoint = "Mohilev", DestinPoint = "Minsk", TrainNumber = "444" }
				};

			ordersList = new List<Order> 
				{
					new Order { Id = 1, BuyerId = 1, OrderDateTime = new DateTime(2014, 10, 12) },
					new Order { Id = 2, BuyerId = 1, OrderDateTime = new DateTime(2014, 10, 11) },
					new Order { Id = 3, BuyerId = 3, OrderDateTime = new DateTime(2013, 10, 12) }
				};

			ticketsList = new List<Ticket> 
				{
					new Ticket { Id = 1, DepartPoint = "Minsk", DestinPoint = "Moscow", TrainId = 1, Price = 20.0, OrderId = 1, Status = TicketStatus.Bought },
					new Ticket { Id = 2, DepartPoint = "Minsk", DestinPoint = "Orsha", TrainId = 1, Price = 10.0, OrderId = 1, Status = TicketStatus.Reserved },
					new Ticket { Id = 3, DepartPoint = "Brest", DestinPoint = "Homel", TrainId = 2, Price = 15.0 },
					new Ticket { Id = 4, DepartPoint = "Mohilev", DestinPoint = "Minsk", TrainId = 4, Price = 5.0 },
				};

			repositoryFactory = new RepositoryFactory();
			
			foreach (Buyer buyer in buyersList)
				repositoryFactory.BuyersRepository.Create(buyer);

			foreach (Train train in trainsList)
				repositoryFactory.TrainsRepository.Create(train);

			foreach (Order order in ordersList)
				repositoryFactory.OrdersRepository.Create(order);

			foreach (Ticket ticket in ticketsList)
				repositoryFactory.TicketsRepository.Create(ticket);

		}

		private List<Buyer> buyersList;
		private List<Order> ordersList;
		private List<Ticket> ticketsList;
		private List<Train> trainsList;

		[TestMethod]
		public void InquiryCtrl_GetTrains_TrainsForDestPoint()
		{
			// Arrange.
			// See InitTest method for source data.
			string destPoint = "Minsk";
			object locker = new object();
			InquiryService inquiryCtrl = new InquiryService(repositoryFactory, locker);
			List<Train> expectedTrains = (from train in trainsList
									   join ticket in ticketsList
									   on train.Id equals ticket.TrainId
									   where ticket.DestinPoint.Contains(destPoint)
									   select train).ToList();

			// Act.
			//List<Train> foundTrains = inquiryCtrl.GetTrains(destPoint).ToList();
			List<Train> foundTrains = inquiryCtrl
				.GetTrains(ticket => ticket.DestinPoint.Contains(destPoint))
				.ToList();

			List<Train> res = foundTrains.Except(expectedTrains).ToList();
			
				// Assert.
			Assert.AreEqual(0, res.Count, "Fail in trains list with pointed destiantion.");
		}

		[TestMethod]
		public void InquiryCtrl_GetTrains_WithMinPrice7()
		{
			// Arrange.
			// See InitTest method for source data.
			double minPrice = 7.0;
			object locker = new object();
			InquiryService inquiryCtrl = new InquiryService(repositoryFactory, locker);
			List<Train> expectedTrains = (from train in trainsList
										  join ticket in ticketsList
										  on train.Id equals ticket.TrainId
										  where ticket.Price >= minPrice
										  select train).ToList();

			// Act.
			List<Train> foundTrains = inquiryCtrl
				.GetTrains(ticket => ticket.Price >= minPrice)
				.ToList();

			List<Train> res = foundTrains.Except(expectedTrains).ToList();

			// Assert.
			Assert.AreEqual(0, res.Count, "Fail in trains list with pointed min price 7.");
		}

		[TestMethod]
		public void InquiryCtrl_GetTickets_AvailableOnly()
		{
			// Arrange.
			// See InitTest method for source data.
			TicketStatus ticketStatus = TicketStatus.Available;
			object locker = new object();
			InquiryService inquiryCtrl = new InquiryService(repositoryFactory, locker);
			List<Ticket> expectedTickets = (from ticket in ticketsList
										  where ticket.Status == ticketStatus
										  select ticket).ToList();

			// Act.
			List<Ticket> foundTickets = inquiryCtrl
				.GetTickets(ticket => ticket.Status == TicketStatus.Available)
				.ToList();

			List<Ticket> res = foundTickets.Except(expectedTickets).ToList();

			// Assert.
			Assert.AreEqual(0, res.Count, "Fail in tickets list with pointed ticket status.");
		}

		[TestMethod]
		public void SalesCtrl_SaleTickets_AllTicketsAvailable()
		{
			// Arrange.
			// See InitTest method for source data.
			Mock<IPaymentProvider> paymentProviderMock = new Mock<IPaymentProvider>();
			object locker = new object();

			paymentProviderMock.Setup(pp => pp.Pay(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<double>()))
				.Returns(new PaymentResult { Result = PaymentResults.Success });

			SalesService salesCtrl = new SalesService(repositoryFactory, paymentProviderMock.Object, locker);
			List<Ticket> buyersTickets = new List<Ticket>
				{
					ticketsList[2],
					ticketsList[3]
				};

			ExitCode expectedExitCode = new ExitCode()
				{
					Code = ExitCodes.Success
				};

			// Act.
			ExitCode exitCode = salesCtrl.SaleTickets(buyersList[0], buyersTickets);

			// Assert
			Assert.AreEqual(expectedExitCode.Code, exitCode.Code, 
				"SalesCtrl returns wrong exit code for available tickets.");
		}

		[TestMethod]
		public void SalesCtrl_SaleTickets_OneTicketWasAlreadyBought()
		{
			// Arrange.
			// See InitTest method for source data.
			Mock<IPaymentProvider> paymentProviderMock = new Mock<IPaymentProvider>();
			object locker = new object();

			paymentProviderMock.Setup(pp => pp.Pay(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<double>()))
				.Returns(new PaymentResult { Result = PaymentResults.Success });

			SalesService salesCtrl = new SalesService(repositoryFactory, paymentProviderMock.Object, locker);
			List<Ticket> buyersTickets = new List<Ticket>
				{
					ticketsList[0],
					ticketsList[3]
				};

			ExitCode expectedExitCode = new ExitCode()
			{
				Code = ExitCodes.Fail
			};

			// Act.
			ExitCode exitCode = salesCtrl.SaleTickets(buyersList[0], buyersTickets);

			// Assert
			Assert.AreEqual(expectedExitCode.Code, exitCode.Code, 
				"SalesCtrl returns wrong exit code for tickets list with one already bought.");
		}
	}
}
