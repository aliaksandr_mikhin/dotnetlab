﻿namespace TicketOffice.Domain.Enums
{
	public enum TicketStatus
	{
		Available = 0,
		Reserved = 1,
		Bought = 2
	}
}
