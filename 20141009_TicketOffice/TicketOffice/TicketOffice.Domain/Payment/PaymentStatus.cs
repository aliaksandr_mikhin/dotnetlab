﻿namespace TicketOffice.Domain.Payment
{
	public enum PaymentStatus
	{
		Success = 0,
		Fail = 1
	}

	public class PaymentStatus
	{
		public PaymentStatus PaymentStatus { get; set; }
		public int PurchaseId { get; set; }
	}
}
