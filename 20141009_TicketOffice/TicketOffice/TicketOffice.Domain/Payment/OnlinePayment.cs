﻿namespace TicketOffice.Domain.Payment
{
	public class OnlinePayment
	{
		const int CARD_LENGTH = 12;
		public PaymentStatus Pay(string cardNumber, double sum, int purchaseId)
		{
			PaymentStatus paymentStatus = new PaymentStatus()
				{
					PurchaseId = purchaseId,
					PaymentStatus = PaymentStatus.Success
				};

			if (cardNumber.Length != CARD_LENGTH)
				paymentStatus.PaymentStatus = PaymentStatus.Fail;

			return paymentStatus;
		}
	}
}
