﻿namespace TicketOffice.Domain.Providers
{
	public interface IPaymentProvider
	{
		PaymentResult Pay(string cardNumber, string order, double sum);
	}
}
