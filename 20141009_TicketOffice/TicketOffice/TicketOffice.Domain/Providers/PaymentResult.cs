﻿namespace TicketOffice.Domain.Providers
{
	public enum PaymentResults
	{
		Success = 0,
		Fail = 1
	}
	
	
	public class PaymentResult
	{
		public PaymentResults Result { get; set; }
		public string Order { get; set; }
	}
}
