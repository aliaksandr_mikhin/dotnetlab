﻿namespace TicketOffice.Domain.Infrastructure
{
	public enum ExitCodes
	{
		Success = 0,
		Fail = 1
	}

	public class ExitCode
	{
		public ExitCodes Code { get; set; }
		public string Message { get; set; }
	}
}
