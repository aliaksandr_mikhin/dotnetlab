﻿namespace TicketOffice.Domain.Entities
{
	public class Train : IEntity<int>
	{
		public int Id { get; set; }
		public string TrainNumber { get; set; }
		public string DestinPoint { get; set; }
		public string DepartPoint { get; set; }
	}
}
