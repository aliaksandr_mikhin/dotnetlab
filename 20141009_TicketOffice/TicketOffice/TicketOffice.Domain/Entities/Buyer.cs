﻿namespace TicketOffice.Domain.Entities
{
	public class Buyer : IEntity<int>
	{
		public int Id { get; set; }
		public string CardNumber { get; set; }
		public string BuyerName { get; set; }
	}
}
