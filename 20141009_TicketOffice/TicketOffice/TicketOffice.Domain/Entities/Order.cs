﻿using System;

namespace TicketOffice.Domain.Entities
{
	public class Order : IEntity<int>
	{
		public int Id { get; set; }
		public DateTime OrderDateTime { get; set; }
		public int BuyerId { get; set; }
	}
}
