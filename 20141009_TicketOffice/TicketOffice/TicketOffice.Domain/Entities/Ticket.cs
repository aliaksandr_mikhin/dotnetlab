﻿using System;
using TicketOffice.Domain.Enums;

namespace TicketOffice.Domain.Entities
{
	public class Ticket : IEntity<int>
	{
		public int Id { get; set; }
		public DateTime DepartureDateTime { get; set; }
		public string DestinPoint { get; set; }
		public string DepartPoint { get; set; }
		public string SeatNumber { get; set; }
		public string CarriageNumber { get; set; }
		public int TrainId { get; set; }
		public string BuyerName { get; set; }
		public double Price { get; set; }
		public TicketStatus Status { get; set; }
		public int OrderId { get; set; }
	}
}