﻿using System;
using System.Collections.Generic;
using System.Linq;
using TicketOffice.Domain.Entities;

namespace TicketOffice.Domain.Repositories
{
	public class DictionaryBasedGenericRepository<TEntity, TKey> :
		IGenericRepository<TEntity, TKey>
		where TEntity : class, IEntity<TKey> 
	{
		Dictionary<TKey, TEntity> entitiesDictionary;

		public DictionaryBasedGenericRepository()
		{
			entitiesDictionary = new Dictionary<TKey, TEntity>();
		}

		public IQueryable<TEntity> All()
		{
			return entitiesDictionary.Values.AsQueryable();
		}

		public TEntity Find(TKey key)
		{
			return entitiesDictionary[key];
		}

		public TEntity Find(Func<TEntity, bool> function)
		{
			return entitiesDictionary
				.FirstOrDefault(e => function(e.Value))
				.Value;
		}

		public TEntity Delete(TKey key)
		{
			TEntity entityToDelete = entitiesDictionary[key];
			entitiesDictionary.Remove(key);
			return entityToDelete;
		}

		public TEntity Delete(TEntity entity)
		{
			TEntity entityToDelete = entitiesDictionary
				.FirstOrDefault(e => e.Key.Equals(entity.Id))
				.Value;

			if (entityToDelete != null)
				entitiesDictionary.Remove(entityToDelete.Id);

			return entityToDelete;
		}

		public TEntity Create(TEntity entity)
		{
			entitiesDictionary.Add(entity.Id, entity);
			return entity;
		}

		public void Update(TEntity entity)
		{
			TEntity entityToUpdate = Find(entity.Id);

			if (entityToUpdate != null)
			{
				entitiesDictionary[entity.Id] = entity;
			}
		}
	}
}
