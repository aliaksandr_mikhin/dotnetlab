﻿using System;
using System.Collections.Generic;
using System.Linq;
using TicketOffice.Domain.Entities;

namespace TicketOffice.Domain.Repositories
{
	public class ListBasedGenericRepository<TEntity, TKey> :
		IGenericRepository<TEntity, TKey>
		where TEntity : class, IEntity<TKey> 
	{
		List<TEntity> entitiesList;

		public ListBasedGenericRepository()
		{
			entitiesList = new List<TEntity>();
		}

		public IQueryable<TEntity> All()
		{
			return entitiesList.AsQueryable<TEntity>();
		}

		public TEntity Find(TKey key)
		{
			return entitiesList.Find(m => m.Id.Equals(key));
		}

		public TEntity Find(Func<TEntity, bool> function)
		{
			Predicate<TEntity> predicate = new Predicate<TEntity>(function);
			return entitiesList.Find(predicate);
		}

		public TEntity Delete(TKey key)
		{
			TEntity entityToDelete = Find(key);

			if (entityToDelete != null)
				entitiesList.Remove(entityToDelete);

			return entityToDelete;
		}

		public TEntity Delete(TEntity entity)
		{
			if (entity != null)
				entitiesList.Remove(entity);

			return entity;
		}

		public TEntity Create(TEntity entity)
		{
			entitiesList.Add(entity);
			return entity;
		}

		public void Update(TEntity entity)
		{
			TEntity entityToFind = Find(entity.Id);

			if (entityToFind != null)
			{
				entitiesList.Remove(Find(entity.Id));
				Create(entity);
			}
		}
	}
}
