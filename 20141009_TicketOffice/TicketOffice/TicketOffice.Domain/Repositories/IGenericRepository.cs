﻿using System;
using System.Linq;

namespace TicketOffice.Domain.Repositories
{
	public interface IGenericRepository<TEntity, TKey> where TEntity : class
	{
		IQueryable<TEntity> All();
		TEntity Find(TKey key);
		TEntity Find(Func<TEntity, bool> function);
		TEntity Delete(TKey key);
		TEntity Delete(TEntity entity);
		TEntity Create(TEntity entity);
		void Update(TEntity entity);
	}
}