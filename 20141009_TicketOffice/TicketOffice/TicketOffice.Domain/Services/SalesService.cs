﻿using System;
using System.Collections.Generic;
using System.Linq;
using TicketOffice.Domain.Entities;
using TicketOffice.Domain.Enums;
using TicketOffice.Domain.Factories;
using TicketOffice.Domain.Infrastructure;
using TicketOffice.Domain.Providers;

namespace TicketOffice.Domain.Services
{
	public class SalesService
	{
		IRepositoryFactory repositoryFactory;
		IPaymentProvider paymentProvider;
		object locker;


		public SalesService(IRepositoryFactory repositoryFactory,
			IPaymentProvider paymentProvider,
			object locker)
		{
			this.repositoryFactory = repositoryFactory;
			this.paymentProvider = paymentProvider;
			this.locker = locker;
		}

		public ExitCode SaleTickets(Buyer buyer, IEnumerable<Ticket> ticketsSet)
		{
			ExitCode exitCode = new ExitCode()
				{
					Code = ExitCodes.Success
				};

			Ticket[] sourceTickets = ticketsSet.ToArray();
			double orderSum = 0;

			PaymentResult paymentResult = new PaymentResult 
				{ 
					Result = PaymentResults.Fail 
				};

			lock (locker)
			{
				Order newOrder = new Order() 
					{
						BuyerId = buyer.Id,

						Id = ++repositoryFactory
							.OrdersRepository
							.All()
							.LastOrDefault()
							.Id,

						OrderDateTime = DateTime.Now
					};

				repositoryFactory.OrdersRepository.Create(newOrder);
				
				foreach (Ticket ticket in ticketsSet)
				{
					ticket.BuyerName = buyer.BuyerName;
					if (ticket.Status == TicketStatus.Bought)
					{
						exitCode.Code = ExitCodes.Fail;
						exitCode.Message = "Ticket with Id " + ticket.Id + 
							" has already been sold.";
						break;
					}
					ticket.Status = TicketStatus.Bought;
					orderSum += ticket.Price;
				}

				if (exitCode.Code == ExitCodes.Success)
					paymentResult = paymentProvider
						.Pay(buyer.CardNumber, newOrder.Id.ToString(), orderSum);

				if (exitCode.Code == ExitCodes.Fail
						|| paymentResult.Result == PaymentResults.Fail)
				{
					int i = 0;

					foreach (Ticket ticket in ticketsSet)
					{
						ticket.BuyerName = sourceTickets[i].BuyerName;
						ticket.Status = sourceTickets[i].Status;
						i++;
					}

					repositoryFactory.OrdersRepository.Delete(newOrder);
				}
			}
			
			return exitCode;
		}
	}
}
