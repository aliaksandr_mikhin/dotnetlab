﻿using System;
using System.Collections.Generic;
using System.Linq;
using TicketOffice.Domain.Entities;
using TicketOffice.Domain.Enums;
using TicketOffice.Domain.Factories;
using TicketOffice.Domain.Infrastructure;

namespace TicketOffice.Domain.Services
{
	public class InquiryService
	{
		IRepositoryFactory repositoryFactory;
		object locker;

		public InquiryService(IRepositoryFactory repositoryFactory, object locker)
		{
			this.repositoryFactory = repositoryFactory;
			this.locker = locker;
		}

		public IEnumerable<Train> GetTrains(Func<Ticket, bool> predicate)
		{
			IEnumerable<Train> trainsSet;

			lock (locker)
			{
				trainsSet = from train in repositoryFactory.TrainsRepository.All()
							join ticket in repositoryFactory.TicketsRepository.All()
							on train.Id equals ticket.TrainId
							where (predicate(ticket))
							select train;
			}

			return trainsSet.ToList();
		}
		
		public IEnumerable<Ticket> GetTickets(Func<Ticket, bool> predicate)
		{
			IEnumerable<Ticket> ticketsSet;

			lock (locker)
			{
				ticketsSet = repositoryFactory.TicketsRepository.All()
					.Where(predicate);
			}

			return ticketsSet.ToList();
		}
	}
}
