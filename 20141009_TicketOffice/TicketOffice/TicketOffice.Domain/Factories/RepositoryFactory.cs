﻿using TicketOffice.Domain.Entities;
using TicketOffice.Domain.Repositories;

namespace TicketOffice.Domain.Factories
{
	public class RepositoryFactory : IRepositoryFactory
	{
		private IGenericRepository<Train, int> trainsRepository;
		private IGenericRepository<Buyer, int> buyersRepository;
		private IGenericRepository<Ticket, int> ticketsRepository;
		private IGenericRepository<Order, int> ordersRepository;

		public IGenericRepository<Ticket, int> TicketsRepository
		{
			get
			{
				return ticketsRepository = ticketsRepository
					?? new DictionaryBasedGenericRepository<Ticket, int>();
			}
		}

		public IGenericRepository<Train, int> TrainsRepository
		{
			get
			{
				return trainsRepository = trainsRepository
					?? new DictionaryBasedGenericRepository<Train, int>();
			}
		}

		public IGenericRepository<Buyer, int> BuyersRepository
		{
			get
			{
				return buyersRepository = buyersRepository
					?? new DictionaryBasedGenericRepository<Buyer, int>();
			}
		}

		public IGenericRepository<Order, int> OrdersRepository
		{
			get
			{
				return ordersRepository = ordersRepository
					?? new DictionaryBasedGenericRepository<Order, int>();
			}
		}
	}
}
