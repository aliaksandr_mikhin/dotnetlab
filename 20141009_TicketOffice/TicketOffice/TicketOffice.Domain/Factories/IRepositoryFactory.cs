﻿using TicketOffice.Domain.Repositories;
using TicketOffice.Domain.Entities;

namespace TicketOffice.Domain.Factories
{
	public interface IRepositoryFactory
	{
		IGenericRepository<Ticket, int> TicketsRepository { get; }
		IGenericRepository<Train, int> TrainsRepository { get; }
		IGenericRepository<Buyer, int> BuyersRepository { get; }
		IGenericRepository<Order, int> OrdersRepository { get; }
	}
}
